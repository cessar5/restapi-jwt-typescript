"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = (0, express_1.Router)();
const verifyTokens_1 = require("../libs/verifyTokens");
const auth_controller_1 = require("../controllers/auth.controller");
//router.get('/', (req,res) => {
//    res.send('hola mundo')
//})
router.post('/signup', auth_controller_1.signup);
router.post('/signin', auth_controller_1.signin);
router.get('/profile', verifyTokens_1.TokenValidation, auth_controller_1.profile);
exports.default = router;
//# sourceMappingURL=auth.js.map