import {Request, Response} from 'express'
import User, {IUser} from '../models/User'

import jwt from 'jsonwebtoken';

export const signup = async (req: Request, res: Response) => {
    //console.log(req.body);
    //guardar nuevo usuario
    const user: IUser = new User({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password

    });

    user.password = await user.encryptPassword(user.password);
    const savedUser = await user.save(); //esto es asincrono
    // creando token
    // variablesecreta se debe leer desde una variable de entorno
    const token: string = jwt.sign({_id: savedUser._id},process.env.TOKEN_SECRET || 'tokentest');
    console.log(savedUser);
    res.header('auth-token',token).json(savedUser);
};

export const signin = async (req: Request, res:Response) => {
    //res.send('signin');
    //console.log(req.body);
    const user  = await User.findOne({email: req.body.email});
    if (!user) return res.status(400).json('email or password is incorrect');

    const correctPassword: boolean = await user.validatePassword(req.body.password);
    if (!correctPassword) return res.status(400).json('Invalid Password');

    const token: string  = jwt.sign({_id: user._id}, process.env.TOKEN_SECRET || 'tokentest',{
        expiresIn: 60 * 60 * 24 //un dia para expirar
    })
    res.header('auth-token',token).json(user);
};

export const profile = async (req: Request, res:Response) => {
    //console.log(req.header('auth-token'));
    //if (req.header('auth-token')){
    //  console.log('Data');
    //}else {
      //  res.send('profile');
    //}
    const user = await User.findById(req.userId, {password : 0});
    if (!user) return res.status(404).json('No user found');
    res.json(user);
};