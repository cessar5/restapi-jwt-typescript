import { Router } from "express";

const router: Router = Router();

import { TokenValidation } from "../libs/verifyTokens";

import {signup, signin, profile } from '../controllers/auth.controller'
//router.get('/', (req,res) => {
//    res.send('hola mundo')
//})

router.post('/signup', signup);
router.post('/signin', signin);
router.get('/profile', TokenValidation, profile);

export default router;